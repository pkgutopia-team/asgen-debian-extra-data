#!/bin/bash
#
# Script for updating AppStream metadata.
# This script is run daily by a cronjob.
#

set -e
set -o pipefail
set -u

# import the general variable set.
. "/srv/appstream.debian.org/scripts/vars"
update_git

# only run one instance of the script
LOCKFILE="$WORKSPACE_DIR/.lock"
cleanup() {
    rm -f "$LOCKFILE"
}

if ! lockfile -r8 $LOCKFILE; then
    echo "aborting AppStream metadata extraction because $LOCKFILE has already been locked"
    exit 0
fi
trap cleanup 0

# start logging
logdir="$WORKSPACE_DIR/public/logs/`date "+%Y/%m"`"
mkdir -p $logdir
NOW=`date "+%d_%H%M"`
LOGFILE="$logdir/${NOW}.log"
exec >> "$LOGFILE" 2>&1

cd $WORKSPACE_DIR

# generate fresh metadata
for component in $COMPONENTS; do
    for suite in $SUITES; do
        $GENERATOR_DIR/bin/appstream-generator process $suite $component
    done
done

# sync files for the ftpmasters (additional precaution so we don't ever break dak/apt)
rsync -a --delete-after $WORKSPACE_DIR/public/data/ /srv/appstream.debian.org/ftpmaster/data/

# finish logging
exec > /dev/null 2>&1

# update CDN copy
static-update-component appstream.debian.org
